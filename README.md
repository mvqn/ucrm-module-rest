# ucrm-plugin-rest
This library is a helper package for interacting with the UCRM REST API.
This package is not a Plugin itself, but will greatly help in the development of actual UCRM Plugins. 

## Installation
Install the latest version with
```bash
$ composer require mvqn/ucrm-plugin-rest
```

## Basic Usage
```php
<?php

// COMING SOON
```

## Documentation
COMING SOON

## Third Party Packages
COMING SOON

## About

### Requirements
- This package will be maintained in step with the PHP version used by UCRM to ensure 100% compatibility.
- This package does not require any PHP extensions that are not already enabled in the default UCRM installation.

### Related Packages
[ucrm-plugin-core](https://bitbucket.org/mvqn/ucrm-plugin-core)\
The core plugin package used by all UCRM Plugins developed internally.

[ucrm-plugin-data](https://bitbucket.org/mvqn/ucrm-plugin-data)\
Another plugin package used to access the UCRM database directly.

### Submitting bugs and feature requests
Bugs and feature request are tracked on [Bitbucket](https://bitbucket.org/mvqn/ucrm-plugin-rest/issues)

### Author
Ryan Spaeth <[rspaeth@mvqn.net](mailto:rspaeth@mvqn.net)>

### License
Monolog is licensed under the MIT License - see the `LICENSE` file for details.

### Acknowledgements
Credit to the Ubiquiti Team for giving us the luxury of Plugins!