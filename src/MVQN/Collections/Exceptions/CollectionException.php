<?php
declare(strict_types=1);

namespace MVQN\Collections\Exceptions;

/**
 * Class CollectionException
 *
 * @package MVQN\Collections
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
class CollectionException extends \Exception
{
}