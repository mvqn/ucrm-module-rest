<?php
declare(strict_types=1);

namespace UCRM\REST\Exceptions;

/**
 * Class RestClientException
 *
 * @package UCRM\REST\Exceptions
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
class RestClientException extends \Exception
{
}