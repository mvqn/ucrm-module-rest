<?php
declare(strict_types=1);

namespace UCRM\REST\Exceptions;

/**
 * Class RestObjectException
 *
 * @package UCRM\REST\Exceptions
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
class RestObjectException extends \Exception
{
}